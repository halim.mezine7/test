<?php 
session_start();
try {
    $bdd = new PDO('mysql:host=localhost;dbname=testdev;charset=utf8', 'root', ''); 
}
// En cas d'erreur de connexion à MySQL,
// on "attrape" l'erreur avec l'objet PDOException dans le bloc catch
catch(PDOException $error){
    echo $error->getCode().' '.$error->getMessage();
 }


if (isset($_POST['boutonCommerce'])) {
    $name = htmlspecialchars(trim($_POST['name']));


$req = $bdd->prepare('INSERT INTO commerce(name) VALUES(:name)');
$req->execute(array(
      'name' => $name
       ));
//Insertion des données dans la bdd

}
$arrayMoyenne = array();
$queryExo2 = $bdd->prepare('SELECT AVG(d.note) as moyenneNote, c.name as nameCommerce, d.commerce as idCommerce, d.fcom as nomCommentaire
FROM data as d
inner join commerce as c
on d.commerce = c.commerce
GROUP BY d.commerce');
$queryExo2->execute();
while($row = $queryExo2->fetch(PDO::FETCH_ASSOC))
{
    array_push($arrayMoyenne,$row);
} 


?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nouveau Commerce</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>

<body>
    <?php include('nav.php') ?>

    <table>
        <tr>
            <th>Nom du commerce</th>
            <th>Note</th>
            <th>Commentaires</th>
        </tr>
        <?php foreach($arrayMoyenne as $item)
            echo '<tr><td>'.$item['nameCommerce'].'</td><td>'.$item['moyenneNote'].'</td><td>'.$item['nomCommentaire'].'</td></tr>';
        ?>
    </table>

</body>

</html>