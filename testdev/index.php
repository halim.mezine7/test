<?php 
session_start();
try {
    $bdd = new PDO('mysql:host=localhost;dbname=testdev;charset=utf8', 'root', ''); 
}
// En cas d'erreur de connexion à MySQL,
// on "attrape" l'erreur avec l'objet PDOException dans le bloc catch
catch(PDOException $error){
    echo $error->getCode().' '.$error->getMessage();
 }


if (isset($_POST['boutonTerritoire'])) {
    $name = htmlspecialchars(trim($_POST['name']));


$req = $bdd->prepare('INSERT INTO territoire(name) VALUES(:name)');
$req->execute(array(
      'name' => $name
       ));
//Insertion des données dans la bdd

}

?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nouveau Territoire</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <?php include('nav.php') ?>

    <form action="#" method="POST">

        <fieldset>
            <table>
                <legend style="color: black">Ajout d'un territoire</legend>
                <tr>
                    <th><br><label for="name">Nom du territoire : </label></th>
                    <td><br><input name="name" /></td>
                </tr>
                <tr>
                    <td><br><button type="submit" name="boutonTerritoire" class="btn btn-primary"><b>Ajouter un
                                territoire</b></button></td>
                </tr>

            </table>
        </fieldset>

    </form>


</body>

</html>