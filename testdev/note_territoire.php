<?php 
session_start();
try {
    $bdd = new PDO('mysql:host=localhost;dbname=testdev;charset=utf8', 'root', ''); 
}
// En cas d'erreur de connexion à MySQL,
// on "attrape" l'erreur avec l'objet PDOException dans le bloc catch
catch(PDOException $error){
    echo $error->getCode().' '.$error->getMessage();
}

$arrayMoyenne = array();
$queryExo2 = $bdd->prepare('SELECT AVG(d.note) as moyenneNote, t.name as nomTerritoire, d.territoire as idTerritoire
    FROM data as d
    inner join territoire as t
    on d.territoire = t.idTerritoire
    GROUP BY d.territoire');
$queryExo2->execute();
while($row = $queryExo2->fetch(PDO::FETCH_ASSOC))
{
    array_push($arrayMoyenne,$row);
} 


?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nouveau Territoire</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<?php include('nav.php') ?>

<body>
    <table>
        <tr>
            <th>Nom du territoire</th>
            <th>Note</th>
        </tr>
        <?php foreach($arrayMoyenne as $item)
                echo '<tr><td>'.$item['nomTerritoire'].'</td><td>'.$item['moyenneNote'].'</td></tr>';
            ?>
    </table>
</body>

</html>